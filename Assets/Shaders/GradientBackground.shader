﻿Shader "Custom/GradientBackground" {
     Properties {
         _ColorA ("ColorA", Color) = (0.145, 0.012, 0.094)
         _ColorB ("ColorB", Color) = (0.122, 0.071, 0.02)
         _ColorC ("ColorC", Color) = (0.,0.,0.,0.)
         _Middle1 ("Middle1", Range(0.001, 0.999)) = 0.3
         _Middle2 ("Middle2", Range(0.001, 0.999)) = 0.6
         _Center ("Center of the Planet", Vector) = (0.5, 0.5, 0., 0.)
         _RotationSpeed("RotationSpeed", Float) = 1
     }

     SubShader {
         Tags {
         "Queue"="Background"  
         "IgnoreProjector"="True"
         }
         
         LOD 100

         ZWrite On

         Pass {
         CGPROGRAM
         
         #pragma vertex vert
         #pragma fragment frag
         #include "UnityCG.cginc"
         #define TWO_PI 6.28318530718

         struct appdata {
             float4 pos: POSITION;
             fixed4 color: COLOR;
             float3 normal: NORMAL;
             float4 uv : TEXCOORD0;
         };

         struct v2f {
             float4 pos : SV_POSITION;
             float4 uv : TEXCOORD0;
         };
         
         
         fixed4 _ColorA;
         fixed4 _ColorB;
         fixed4 _ColorC;
         float  _Middle1;
         float  _Middle2;
         float3 _Center;
         float _RotationSpeed;
       
         v2f vert (appdata v) {
             v2f o;
             o.pos = UnityObjectToClipPos (v.pos);
             o.uv = v.uv;
             return o;
         }

         fixed4 frag (v2f i) : COLOR {
             
             float2 toCenter = _Center - i.uv;
             float angle = atan2(toCenter.y,toCenter.x);
             float radius = length(toCenter)*.5;
             float d = (angle/TWO_PI)+0.5;
             
             //angle gradient
             fixed4 c = lerp(_ColorA, _ColorB, (d/_Middle1)) * step(0, d) * step(d, _Middle1) ;
             c += lerp(_ColorB, _ColorC, (d - _Middle1)/(_Middle2 - _Middle1) ) * step(_Middle1, d) * step(d, _Middle2);
             c += lerp(_ColorC, _ColorA, (d - _Middle2)/(1.0 - _Middle2) ) * step(_Middle2, d) * step(d, 1.1);
             
             //radial gradient
             c -= 0.004/radius; 
             
             return c;
         }
         
         ENDCG
         }
     }
 }
