﻿/*Exports current value of parameters into a JSON 
file and captures a screenshot*/

using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;


public class SaveAsJson : MonoBehaviour {

    public InputField jsonNameInputField;

    public void SaveJSON(){
        GraphicParameters paramToSave = GetComponent<ParticleCloud>().Params;
        string parameterDataString = JsonUtility.ToJson(paramToSave, true);

        string filePath;
        string imagePath;

        filePath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
        imagePath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);

        //Set file name to current time if no user input is detected
        if (String.IsNullOrEmpty(jsonNameInputField.text))
        {
            string currentTime = string.Format("{0:yyyy-MM-dd_hh-mm-ss-fff}", DateTime.Now);
            filePath += "/" + currentTime + ".json";
            imagePath += "/" + currentTime + ".png";
        }
        else
        {
            filePath += "/" + jsonNameInputField.text + ".json";
            imagePath += "/" + jsonNameInputField.text + ".png";
        }

        ScreenCapture.CaptureScreenshot(imagePath);

        StreamWriter newFile = new StreamWriter(filePath);
        newFile.WriteLine(parameterDataString);
        newFile.Close();
        UnityEditor.AssetDatabase.Refresh();

        ResetInputField();
    }

    void ResetInputField(){
        jsonNameInputField.text = "";
    }
}
