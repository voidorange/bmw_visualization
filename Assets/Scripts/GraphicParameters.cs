﻿using System;

[Serializable]
public class GraphicParameters
{
    public float sizeFactor;
    public float speedFactor;
    public float randomLevel;
    public float extrudeLevel;
    public float brightnessFactor;
    public float radius;
    public int recursionLevel;


    //Deep copy
    public GraphicParameters Copy()
    {
        GraphicParameters copy = new GraphicParameters();
        copy.sizeFactor = this.sizeFactor;
        copy.speedFactor = this.speedFactor;
        copy.randomLevel = this.randomLevel;
        copy.extrudeLevel = this.extrudeLevel;
        copy.brightnessFactor = this.brightnessFactor;
        copy.radius = this.radius;
        copy.recursionLevel = this.recursionLevel;
        return copy;
    }
}