﻿/* This script functions as both a generator and 
a controller of the point cloud: 
1. Icoshpere mesh (reference for default position): setup, recursion
2. Particle system: setup, update
3. Animation: particles, gradient background shader
4. Algorithm functions
5. User Interface, listener
6. Quick save and load to binary data file
*/


using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine.UI;


[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(ParticleSystem))]
public class ParticleCloud : MonoBehaviour
{
    //IcoSphere
    #region Polygon Struct
    private struct TriangleIndices
    {
        public int v1;
        public int v2;
        public int v3;

        public TriangleIndices(int v1, int v2, int v3)
        {
            this.v1 = v1;
            this.v2 = v2;
            this.v3 = v3;
        }
    }
    #endregion

    List<Vector3> m_Vertices;
    List<TriangleIndices> faces;

    //Particle System
    ParticleSystem particleSystem;
    ParticleSystem.Particle[] particles;
    Vector3[] initPos;
    int particleCount;
    bool psSetupFinished;


    //Controller
    public GraphicParameters Params { get { return param; } }
    GraphicParameters param;

    //User Interface
    enum SliderTags
    {
        SIZE = 0,
        SPEED = 1,
        RANDOM = 2,
        EXTRUDE = 3,
        BRIGHTNESS = 4,
        RADIUS = 5,
        RECURSION = 6
    };
    [Tooltip("0: Size Slider " +
             "\n1: Speed Slider" +
             "\n2: Random Level Slider" +
             "\n3: Extrude Level Slider" +
             "\n4: Brightness Slider" +
             "\n5: Radius Slider" +
             "\n6: Level of Recursion")]
    public Slider[] sliders = new Slider[7];
    public GameObject uiPanel;


    //background gradient
    public Renderer bgRenderer;
    public Material bgMat;


    //animation
    float animationStartTime;
    GraphicParameters defaultParam;
    bool startAnimation;
    float speed = 1f;
    float animPctInput = 0f;


    void Start()
    {
        //initialize a basic icosphere shape for reference
        InitBasicShapeData(4, 6);

        defaultParam = new GraphicParameters
        {
            sizeFactor = 1f,
            speedFactor = 0.01f,
            randomLevel = 0.35f,
            extrudeLevel = 0.7f,
            brightnessFactor = 2f,
            radius = 1.2f,
            recursionLevel = 4
        };
        param = defaultParam.Copy();
        SetupSliderListeners();

        bgRenderer.material = bgMat;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            StartAnimation();
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            uiPanel.SetActive(!uiPanel.activeSelf);
        }

        if (startAnimation)
        {
            ExplodeAnimation();
            BackgroundGradientAnimation();
        }

        if (psSetupFinished) UpdateParticle(param);

        UpdateSliderData();
    }


    #region Quick Save & Load 
    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/PreferenceData.dat");
        bf.Serialize(file, param);
        file.Close();
    }

    public void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/PreferenceData.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/PreferenceData.dat", FileMode.Open);

            GraphicParameters loadedData = new GraphicParameters();
            loadedData = (GraphicParameters)bf.Deserialize(file);
            file.Close();

            param = loadedData;
            UpdateSliderData();
        }
    }
    #endregion


    #region UI: Setup Sliders and Listeners, User Input
    void SetupSliderListeners()
    {
        for (int i = 0; i < sliders.Length; i++)
        {
            int index = i;
            sliders[index].onValueChanged.AddListener(delegate { SliderValueChangeCheck(sliders[index], index); });
        }
    }


    /// <summary>
    /// Update Parameters' Value to Current Sliders Value
    /// </summary>
    /// <param name="_s">slider</param>
    /// <param name="_SliderIndex">_s' index in sliders array & enum.</param>
    public void SliderValueChangeCheck(Slider _s, int _SliderIndex)
    {
        SliderTags activeSlider = (SliderTags)_SliderIndex;

        switch (activeSlider)
        {
            case SliderTags.SIZE:
                param.sizeFactor = _s.value;
                break;
            case SliderTags.SPEED:
                param.speedFactor = _s.value;
                break;
            case SliderTags.RANDOM:
                param.randomLevel = _s.value;
                break;
            case SliderTags.EXTRUDE:
                param.extrudeLevel = _s.value;
                break;
            case SliderTags.BRIGHTNESS:
                param.brightnessFactor = _s.value;
                break;
            case SliderTags.RADIUS:
                param.radius = _s.value;
                break;
            case SliderTags.RECURSION:
                param.recursionLevel = (int)_s.value;
                InitBasicShapeData(param.recursionLevel, 6);
                break;
            default:
                break;
        }
    }

    void UpdateSliderData()
    {
        sliders[0].value = param.sizeFactor;
        sliders[1].value = param.speedFactor;
        sliders[2].value = param.randomLevel;
        sliders[3].value = param.extrudeLevel;
        sliders[4].value = param.brightnessFactor;
        sliders[5].value = param.radius;
        sliders[6].value = param.recursionLevel;
    }
    #endregion


    #region Animation
    public void StartAnimation()
    {
        if (!startAnimation)
        {
            startAnimation = true;
            animationStartTime = Time.time;
            param = defaultParam.Copy();
            param.speedFactor = 0f;
        }
    }

    void ExplodeAnimation()
    {
        animPctInput = PctInputFromTime(animationStartTime, 20);

        if (animPctInput >= 1f)
        {
            startAnimation = false;
            return;
        }

        float pct = animPctInput <= 0.5 ? EasingCurve(animPctInput, 5f) : ParabolaCurve(animPctInput, 0.6f);
        float speedPct = LogisticSigmoidAlgo(animPctInput, 0.85f);

        transform.RotateAround(transform.position, Vector3.back, speedPct * 0.2f);
        param.speedFactor = PctInterpolation(0f, 0.01f, speedPct);
        param.radius = PctInterpolation(defaultParam.radius, 3f, pct);
        param.extrudeLevel = PctInterpolation(defaultParam.extrudeLevel, -4f, pct);
        param.sizeFactor = PctInterpolation(defaultParam.sizeFactor, 2f, pct);
        param.randomLevel = PctInterpolation(defaultParam.randomLevel, 0.35f, pct);
    }

    void BackgroundGradientAnimation(){
        Color colorAFrom = new Color(0.149f, 0.012f, 0.106f);
        Color colorATo = new Color(0.008f, 0.024f, 0.157f);
        Color cColorA = Color.Lerp(colorAFrom, colorATo, animPctInput);

        Color colorBFrom = new Color(0.125f, 0.086f, 0.008f);
        Color colorBTo = new Color(0.008f, 0.043f, 0.047f);
        Color cColorB = Color.Lerp(colorBFrom, colorBTo, animPctInput);

        Color colorCFrom = new Color(0.047f, 0.031f, 0f);
        Color colorCTo = new Color(0.059f, 0f, 0.004f);
        Color cColorC = Color.Lerp(colorCFrom, colorCTo, animPctInput);

        bgMat.SetColor("_ColorA", cColorA);
        bgMat.SetColor("_ColorB", cColorB);
        bgMat.SetColor("_ColorC", cColorC);
    }

    float PctInputFromTime(float _startTime, float _duration)
    {
        float t = Time.time - _startTime;
        return t / _duration;
    }

    float PctInterpolation(float _origin, float _dest, float _pct)
    {
        return (1 - _pct) * _origin + _pct * _dest;
    }


    //EaseOut algorithm: pct(0.5, 1), k = 0.6
    float ParabolaCurve(float x, float k)
    {
        return Mathf.Pow(4.0f * x * (1.0f - x), k);
    }

    //EaseIn algorithm: pct(0, 0.5), k = 5
    float EasingCurve(float x, float k){
        return 1.0f - Mathf.Pow(Mathf.Abs((x - 0.5f) * 2), k);
    }

    //http://www.flong.com/texts/code/shapers_exp/
    //a = 0.85, for speed acceleration
    float LogisticSigmoidAlgo(float x, float a)
    {
        float epsilon = 0.0001f;
        float min_param_a = 0.0f + epsilon;
        float max_param_a = 1.0f - epsilon;
        a = Mathf.Max(min_param_a, Mathf.Min(max_param_a, a));
        a = (1f / (1f - a) - 1f);

        float A = 1.0f / (1.0f + Mathf.Exp(0f - ((x - 0.5f) * a * 2.0f)));
        float B = 1.0f / (1.0f + Mathf.Exp(a));
        float C = 1.0f / (1.0f + Mathf.Exp(0f - a));
        float y = (A - B) / (C - B);
        return y;
    }
    #endregion


    #region Particle System
    /// <summary>
    /// Initializes the particle system.
    /// </summary>
    public void SetupParticleSystem()
    {
        particleSystem = GetComponent<ParticleSystem>();
        particleCount = m_Vertices.Count;
        particles = new ParticleSystem.Particle[particleCount];

        initPos = new Vector3[particleCount];
        initPos = m_Vertices.ToArray();

        for (int i = 0; i < particleCount; i++)
        {
            particles[i].position = initPos[i];
            particles[i].startColor = Color.white;
            particles[i].startSize = 0.2f;
        }

        particleSystem.SetParticles(particles, particleCount);
        psSetupFinished = true;
    }


    /// <summary>
    /// Updates the particle system & Draw.
    /// </summary>
    /// <param name="_p">Current Graphics Parameters</param>
    void UpdateParticle(GraphicParameters _p)
    {

        GraphicParameters p = _p;
        speed += p.speedFactor;

        for (int i = 0; i < particleCount; i++)
        {

            //noise calculation
            Vector3 noiseInput = p.randomLevel * initPos[i] + new Vector3(speed*1.1f, speed, speed);
            float noise = Perlin.Noise(noiseInput);

            //position
            float displacement = p.extrudeLevel * noise;
            Vector3 normal = (initPos[i] - transform.position).normalized;
            Vector3 newPos = initPos[i] * p.radius + normal * displacement;
            particles[i].position = newPos;

            //color
            float brightness = Map(noise, -0.0f, 0.5f, 0.15f, 1.0f);
            brightness *= p.brightnessFactor;
            particles[i].startColor = new Color(brightness, brightness, brightness, 1f);

            //size
            float size = Map(noise, -0.0f, 0.5f, 0.15f, 0.45f);
            particles[i].startSize = size * p.sizeFactor;

        }

        //update particle system
        particleSystem.SetParticles(particles, particleCount);
    }
    #endregion


    #region Setup Basic Icosphere as a Reference
    //Algorhithm reference: http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
    /// <summary>
    /// Initialize the basic shape data.
    /// </summary>
    /// <param name="_recursionLevel">Recursion level.</param>
    /// <param name="_radius">Radius.</param>
    public void InitBasicShapeData(int _recursionLevel, float _radius)
    {
        psSetupFinished = false;

        m_Vertices = new List<Vector3>();
        faces = new List<TriangleIndices>();

        Dictionary<long, int> middlePointIndexCache = new Dictionary<long, int>();

        int recursionLevel = _recursionLevel;
        float radius = _radius;

        float t = (1.0f + Mathf.Sqrt(5.0f)) / 2.0f;

        //add vertices
        m_Vertices.Add(new Vector3(-1, t, 0).normalized * radius);
        m_Vertices.Add(new Vector3(1, t, 0).normalized * radius);
        m_Vertices.Add(new Vector3(-1, -t, 0).normalized * radius);
        m_Vertices.Add(new Vector3(1, -t, 0).normalized * radius);
        m_Vertices.Add(new Vector3(0, -1, t).normalized * radius);
        m_Vertices.Add(new Vector3(0, 1, t).normalized * radius);
        m_Vertices.Add(new Vector3(0, -1, -t).normalized * radius);
        m_Vertices.Add(new Vector3(0, 1, -t).normalized * radius);
        m_Vertices.Add(new Vector3(t, 0, -1).normalized * radius);
        m_Vertices.Add(new Vector3(t, 0, 1).normalized * radius);
        m_Vertices.Add(new Vector3(-t, 0, -1).normalized * radius);
        m_Vertices.Add(new Vector3(-t, 0, 1).normalized * radius);

        //add faces
        faces.Add(new TriangleIndices(0, 11, 5));
        faces.Add(new TriangleIndices(0, 5, 1));
        faces.Add(new TriangleIndices(0, 1, 7));
        faces.Add(new TriangleIndices(0, 7, 10));
        faces.Add(new TriangleIndices(0, 10, 11));
        faces.Add(new TriangleIndices(1, 5, 9));
        faces.Add(new TriangleIndices(5, 11, 4));
        faces.Add(new TriangleIndices(11, 10, 2));
        faces.Add(new TriangleIndices(10, 7, 6));
        faces.Add(new TriangleIndices(7, 1, 8));
        faces.Add(new TriangleIndices(3, 9, 4));
        faces.Add(new TriangleIndices(3, 4, 2));
        faces.Add(new TriangleIndices(3, 2, 6));
        faces.Add(new TriangleIndices(3, 6, 8));
        faces.Add(new TriangleIndices(3, 8, 9));
        faces.Add(new TriangleIndices(4, 9, 5));
        faces.Add(new TriangleIndices(2, 4, 11));
        faces.Add(new TriangleIndices(6, 2, 10));
        faces.Add(new TriangleIndices(8, 6, 7));
        faces.Add(new TriangleIndices(9, 8, 1));

        //Apply recursion level
        for (int i = 0; i < recursionLevel; i++)
        {
            List<TriangleIndices> newFaces = new List<TriangleIndices>();
            foreach (var tri in faces)
            {
                // replace triangle by 4 triangles
                int a = GetMiddlePoint(tri.v1, tri.v2, ref m_Vertices, ref middlePointIndexCache, radius);
                int b = GetMiddlePoint(tri.v2, tri.v3, ref m_Vertices, ref middlePointIndexCache, radius);
                int c = GetMiddlePoint(tri.v3, tri.v1, ref m_Vertices, ref middlePointIndexCache, radius);

                newFaces.Add(new TriangleIndices(tri.v1, a, c));
                newFaces.Add(new TriangleIndices(tri.v2, b, a));
                newFaces.Add(new TriangleIndices(tri.v3, c, b));
                newFaces.Add(new TriangleIndices(a, b, c));
            }
            faces = newFaces;
        }

        SetupParticleSystem();
    }


    void DrawBasicShapeMeshInPointMode()
    {
        Mesh mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;

        int[] indecies = new int[m_Vertices.Count];
        Color[] colors = new Color[m_Vertices.Count];

        for (int i = 0; i < m_Vertices.Count; ++i)
        {
            indecies[i] = i;
            colors[i] = Color.white;
        }

        mesh.vertices = m_Vertices.ToArray();
        mesh.colors = colors;
        mesh.SetIndices(indecies, MeshTopology.Points, 0);
    }

    /// <summary>
    /// Calculate a new middle point for updating resursion level.
    /// </summary>
    /// <returns>The middle point.</returns>
    /// <param name="p1">vertex 1</param>
    /// <param name="p2">vertex 2</param>
    /// <param name="vertices">current vertices list</param>
    /// <param name="cache">Cache.</param>
    /// <param name="radius">Radius.</param>
    static int GetMiddlePoint(int p1, int p2, ref List<Vector3> vertices, ref Dictionary<long, int> cache, float radius)
    {
        //Cache check
        bool firstIsSmaller = p1 < p2;
        long smallerIndex = firstIsSmaller ? p1 : p2;
        long greaterIndex = firstIsSmaller ? p2 : p1;
        long key = (smallerIndex << 32) + greaterIndex;

        int ret;
        if (cache.TryGetValue(key, out ret))
        {
            return ret;
        }

        //Create a new point
        Vector3 point1 = vertices[p1];
        Vector3 point2 = vertices[p2];
        Vector3 middle = new Vector3
        (
            (point1.x + point2.x) / 2f,
            (point1.y + point2.y) / 2f,
            (point1.z + point2.z) / 2f
        );

        int i = vertices.Count;
        vertices.Add(middle.normalized * radius);

        cache.Add(key, i);
        return i;
    }
    #endregion


    #region MapRange Function
    public float Map(float value, float pfrom, float pTo, float newFrom, float newTo)
    {
        if (value <= pfrom)
            return newFrom;

        if (value >= pTo)
            return newTo;

        return (value - pfrom) * (newTo - newFrom) / (pTo - pfrom) + newFrom;
    }
    #endregion
}
