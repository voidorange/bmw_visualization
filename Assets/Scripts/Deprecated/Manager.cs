﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Manager : MonoBehaviour
{

    Material material;
    public GraphicParameters param;

    void Start()
    {
        Renderer rend = GetComponent<Renderer>();
        rend.material.shader = Shader.Find("Custom/PointCloud (Deprecated)");
        material = rend.material;

        param = new GraphicParameters
        {
            sizeFactor = 1f,
            speedFactor = 0.5f,
            randomLevel = 0.23f,
            extrudeLevel = 3.0f,
            brightnessFactor = 1.0f,
            radius = 1.0f,
            recursionLevel = 1
        };

    }

    void Update()
    {
        UpdateGraphics();
    }

    void UpdateGraphics()
    {
        material.SetFloat("_SizeFactor", param.sizeFactor);
        material.SetFloat("_SpeedFactor", param.speedFactor);
        material.SetFloat("_RandomLevel", param.randomLevel);
        material.SetFloat("_ExtrudeLevel", param.extrudeLevel);
        material.SetFloat("_AlphaFactor", param.brightnessFactor);
        material.SetFloat("_Radius", param.radius);
       
    }

}


