﻿/* 
 * @brief Pointcloud library for .NET
 * @author M. Hofman
 */

#if UNITY_STANDALONE
#define IMPORT_GLENABLE
#endif

using UnityEngine;
using System;
using System.Runtime.InteropServices;

public class SystemGLEnable : MonoBehaviour
{
    const UInt32 GL_VERTEX_PROGRAM_POINT_SIZE = 0x8642;
    const UInt32 GL_POINT_SMOOTH = 0x0B10;
    const UInt32 GL_BLEND = 0x0BE2;

    const string LibGLPath =
#if UNITY_STANDALONE_WIN
        "opengl32.dll";
#elif UNITY_STANDALONE_OSX
    "/System/Library/Frameworks/OpenGL.framework/OpenGL";
#elif UNITY_STANDALONE_LINUX
    "libGL";  // Untested on Linux, this may not be correct
#else
    null;   // OpenGL ES platforms don't require this feature
#endif

#if IMPORT_GLENABLE
    [DllImport(LibGLPath)]
    public static extern void glEnable(UInt32 cap);
    //public static extern void glBlendFunc(GLenum sfactor,GLenum dfactor);

    bool mIsOpenGL;

    void Start()
    {
        mIsOpenGL = SystemInfo.graphicsDeviceVersion.Contains("OpenGL");
    }

    void OnPreRender()
    {
        if (mIsOpenGL)
          glEnable(GL_BLEND);
        //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
            glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
    
            glEnable(GL_POINT_SMOOTH);

    }
#endif
}